def doit(s):
    i = 0
    while True:
        i += 1
        expected = str(i)
        print(expected, s)
        if s.startswith(expected):
            last_val = expected
            s = s[len(expected):]
        else:
            return last_val


if __name__ == '__main__':
    assert doit('12345') == '5'
    assert doit('12345678910111213') == '13'
    assert doit('1235678') == '3'

