from datetime import datetime

def process(data):
    rval = []
    first = datetime.strptime(data[0], '%Y-%m-%d')
    week = 0
    this_week = []
    for d in data:
        w = int((datetime.strptime(d, '%Y-%m-%d') - first).days / 7)
        if w == week:
            this_week.append(d)
        else:
            rval.append(this_week)
            this_week = [d]
            week = w
        print(d, w)
    rval.append(this_week)
    return rval


if __name__ == '__main__':
    test = ['2019-01-01', '2019-01-02', '2019-01-08', '2019-02-01', '2019-02-02', '2019-02-05', '2019-02-06']
    print(process(test))