-- look at scores table
select * from scores;

-- gets data, just need to pull the first row from it (somehow)
select a.name as one_student,
b.name as other_student,
abs(a.score - b.score) as score_diff
from scores a left outer join scores b
where a.id > b.id
order by score_diff

-- gets the row with minimum score from scores table (the hard way)
select s.* from scores s
inner join (
	select id, name, min(score) as min_score
	from scores s
	) new_s on s.id = new_s.id and s.score = new_s.min_score;
	
--gets the row with minimum score from scores table (the easy way)
select id, name, min(score) as min_score
	from scores s;

-- uses the (easy way) design pattern on original data (THE ANSWER)
select one_student, other_student, min(score_diff) as min_score_diff
from (select a.name as one_student,
b.name as other_student,
abs(a.score - b.score) as score_diff
from scores a left outer join scores b
where a.id > b.id);